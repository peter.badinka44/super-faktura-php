<?php

use GuzzleHttp\Client;
use Czechphp\ICOValidator\ICOValidator;

class CompanyDataFetcher
{
	private Client $httpClient;

	private ICOValidator $icoValidator;

	private string $url;

	public function __construct()
	{
		$this->httpClient = new Client();
		$this->icoValidator = new ICOValidator();
		$this->url = "http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_bas.cgi?ico=";
	}

	/**
	 * @return array<mixed>
	 */
	public function fetch(string $ico): array
	{
		$validation = $this->icoValidator->validate($ico);

		if ($validation !== ICOValidator::ERROR_NONE) {
			return $this->response(error: true, message: 'ICO is not valid');
		}

		try {
			$response = $this->httpClient->get($this->url.$ico);
		} catch (\Exception $e) {
			return $this->response(error: true, message: $e->getMessage());
		}

		return $this->response(error: false, data: $response->getBody());
	}

	/**
	 * @return array<mixed>
	 */
	private function response(bool $error, string $message = '', string $data = ''): array
	{
		return [
			'error' => $error,
			'message' => $message,
			'data' => $data,
		];
	}
}